#!/usr/bin/env perl
# convert kiva_lend.pl generated data to CSV for later importing to MySQL.
use Text::CSV;
use 5.010;
use warnings;
use Encode qw/_utf8_off/;
use YAML qw(Dump DumpFile);
use JSON qw(to_json from_json);
use POSIX qw/strftime/;
use Data::Dumper;

#use perl -MData::Dumper -MDBM::Deep -le 'tie %h,"DBM::Deep", "loan.db"; print Dumper $h{687689}'
use DBM::Deep;

my $db_file = shift;
my $table   = shift;

if ( !( $db_file && $table ) ) {
    die "please check your args db_file and table name";
}

# Predisbursed need change
my $mapping = {
    Country => [
        qw( CountryID CountryName Capital OffLang Population AnnIncome
          Agriculture Industry Service Poverty Literacy InfantMort Lifeexpct )
    ],
    Loan => [
        qw(LoanID LoanName Gender LoanAmount GroupYN NoinGroup LoanCategory1 LoanCategory2),
        qw(RepayTerm RepaySchedule Pre-disbursed Listed Currencyloss DefaultProtect Ended PictureYN),
        qw(LoanDescription AdditionalInfo Translator OriginalLang CountryID PartnerID),
        qw(Lenders Teams)
    ],
    LoanGroup  => [qw( LoanID GrpmemberName )],
    LoanMember => [qw( LoanID MemberID )],
    Member     => [
        qw( MemberID MemberName location occupation because about MemberSince picture )
    ],
    Partner => [
        qw(PartnerID    PartnerName RiskRating  PartnerCountry  Dilligence  Startdate   Totalloans  InactAmt    InactNo PaybackAmt  PaybackNo   EndedAmt    EndedNo Delinqrate  ArrearsAmt  OutstndPort DelinqNo    Defaultrate DefEndAmt   DefEndNo    CrrXlossRate    CrrXlossAmt RefundRate  RefundAmt   RefundNo    WomenLoan   Avgsize AvgInd  AvgGrp  AvgEntrp    AvgPPPlocal AvgsizevsPPP    AvgTime2fund    Avg$dayPerLoan  AvgloanTerm totalJournal    JournalRate AvgCommPerJ AcgRecommPerJ   Pyield  ROA AvgsizeperCapIncome)
    ],
    SocialPerf => [qw(PartnerID SocialBadge )],
    Team       => [
        qw(TeamID   TeamName    Category    Since   Because About   Location    Noofmembers Amountloaned    Noofloans   ARankNUAll  ARankNUThisMon  ARankNULastMon  ARankALAll  ARankALThisMon  ARankALLastMon  CRankNUAll  CRankNUThisMon  CRankNULastMon  CRankALAll  CRankALThisMon  CRankALLastMon),
    ],
    TeamLoan   => [qw(TeamID LoanID)],
    TeamMember => [qw(TeamID MemberID Captain)],
};

my $db  = DBM::Deep->new($db_file);

#say Dump $hash{$key};

#open $fh, ">:encoding(utf8)", "new.csv" or die "new.csv: $!";
my $csv = Text::CSV->new( { binary => 1, sep_char => "," } );
$csv->eol("\r\n");

my $fh_set = {};
for my $t ( keys %$mapping ) {
    next unless lc $t eq lc $table;
    open( my $fh, ">:encoding(utf8)", '/tmp/' . $t . ".csv" );
    my @columns = @{ $mapping->{$t} };
    $fh_set->{$t} = $fh;
    print $fh join( ",", @columns ) . "\n";
}
my $json = JSON->new;

#$json->allow_blessed(1);
#$json->allow_nonref(1);
#$json->convert_blessed(1);
mapping_access($db);

sub mapping_access {
    my $hash  = shift;
    my $count = 0;

    say "processing table $table......";
    for my $id ( keys %$hash ) {
        my @row;
        my @columns = @{ $mapping->{$table} };
        my $fh = $fh_set->{$table};

        if ($table eq "TeamLoan") {
                for my $loan (@{$hash->{$id}}) {
                    $csv->print( $fh, [$id, $loan] );
                }

        } elsif ($table eq "TeamMember") {

                my @data;
                for $key (qw/isCaptian teams/) {
                    my %seen;
                    my $isCap = $key eq "isCaptian" ? "Y" : "N";
                    next unless exists $hash->{$id}{$key};
                    for my $team (@{$hash->{$id}{$key}}) {
                        push @data, [$team, $id, $isCap] unless exists $seen{$team};
                        $seen{$team}++;
                    }
                }

                $csv->print( $fh, $_ ) for @data;
        
        
        } elsif ($table eq "SocialPerf") {
       
                my @badge;
                for my $item (@{$hash->{$id}}) {
                    push @badge, $item->{SocialBadge};
                }
                
                $csv->print( $fh, [$id, $_] ) for @badge;

            
        } else {

            for (@columns) {
                my $field = $_;
                $field =~ s/-//g;
                $field = /^(Lenders|Teams)$/ ? lcfirst($field) : $field;

=pod
                    if (($field =~ /^(lenders|teams)$/) && ((ref $hash->{$id}{$field}) =~ /Array/)) {
                        $v = join ",", map { $_->{profile} } @{ $hash->{$id}{$field} };
                        $v =~ s/http:\/\/[^\s,]+\/\b//g;
                    } elsif ($field =~ /^(Predisbursed|Listed)$/) {
                        $v = $hash->{$id}{$field} ? strftime("%Y-%m-%d", gmtime($hash->{$id}{$field})) : $v;
                    } else {
                        $v = $hash->{$id}{$field} ? $hash->{$id}{$field} : $v; 
                    }
=cut

                my $v = $hash->{$id}{$field};
                $v ||= 'N/A';

                $v =~ s/^\s*|\s*$//g;
                $v =~ s/[\r\n]+/ /g;

                _utf8_off $v;

                #my $v = ref $raw ? $json->encode($raw->export() ) : $raw;
                push @row, $v;
            }


        die "columns not matched" unless scalar(@columns) eq scalar(@row);

        #print $fh join(',',map { '"'.$_.'"' } @row)."\n" if @row;
        $csv->print( $fh, [@row] ) if @row;
        $count++;
        }
    }
    close $fh_set->{$_} for keys %$fh_set;
}

__END__

open $fh, ">:encoding(utf8)", "new.csv" or die "new.csv: $!";

