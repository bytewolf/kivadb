#!/usr/bin/env perl
# hui
# scrapy partner's info from kiva.org

use strict;
use Mojo::UserAgent;
use FindBin qw/$Bin/;
use JSON::XS;
use DBM::Deep;
use Data::Dumper;
$|=1;

my $datadir = "$Bin/../data";
my $url = 'http://www.kiva.org/partners';
my $ua = Mojo::UserAgent->new;
#$ua->transactor->name('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
$ua->transactor->name('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0');

print "-> getting partner IDs from $url\n";
my $tx = $ua->get($url);
my $html = $tx->res->body;
my $dom = $tx->res->dom;
my $total = 0;
my @partner_ids;

if ($html =~ /\bpartnerCard\b/) {

   $dom->find('article.partnerCard h1.name > a')->each(
        sub {
            my $r = shift;
            my ($id) = $r->attr('href') =~ /\/(\d+)$/;
            push @partner_ids, $id;
            $total++;
            }
    ); 

} else {
    print Dumper $tx->res;
    die "failed!";
}

print "  - [total partners: $total]\n";


### Main

open F, ">$Bin/../,all_partner_ids" or die "Can't create $Bin/../,all_partner_ids: $!\n";
for my $id (@partner_ids) {
    print F "$id\n";
}
close F;

my @target;
my $active = 0;
my %result;
tie %{$result{partner}},    'DBM::Deep', "$datadir/partner.db";
tie %{$result{country}},    'DBM::Deep', "$datadir/country.db";
tie %{$result{socialperf}}, 'DBM::Deep', "$datadir/socialperf.db";
map { $result{$_} = {} unless ref $result{$_} eq 'HASH' } keys %result;
{
    my $MAX = 5;
    @target = map {'http://www.kiva.org/partners/'.$_} @partner_ids;
    _parallel_get($MAX, \@target, \&_cb_parseResult, \%result);
}

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

### Subs

sub _parallel_get {
    my ( $MAX, $target, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 10 ? $MAX : 10);
    Mojo::IOLoop->recurring(
        0 => sub {

            for ($active + 1 .. $MAX) {

                return($active or Mojo::IOLoop->stop) unless my $url = shift @$target; 
                $active++;
                $ua->get("$url" => sub {
                    my ($ua, $tx) = @_;
                    $callback->($tx, $retval);
                });

            }
        });
}

 
sub _cb_parseResult {
    my ($tx, $retval) = @_;
    my $url  =  $tx->req->url;
    my $html = $tx->res->body;
    my $dom  = $tx->res->dom;
    my ($id) = $url =~ /\/(\d+)$/;
    $active--;

    print " -> scrapping partner details from: $url\n";
    unless ($html =~ /\bField Partner Staff\b/) {
        warn "[err] something wrong with this partner page: $url\n";
        unshift @target, $url;
        return "err";
    }


# Socialperf
    my $socialperf = $retval->{socialperf}{$id} = [];
    if ($html =~ /\/mc\/glossary\//) {
        $dom->find('div.spStrengthItem a')->each(
            sub {
                my $r = shift;
                push @{$socialperf}, {SocialBadge => $r->text, SocialBadgeUrl => $r->attr('href')};
            }
        );
    }

# Partner details
    my $partner = $retval->{partner}{$id} = {};
    $partner->{PartnerID}             = $id;
    $partner->{PartnerName}           = $dom->at('hgroup > h2')->text;
    $partner->{RiskRating}            = $dom->at('div.partnerRating')->attr('title');
    $partner->{RiskRating}            =~ s/Partner Risk Rating:\s*//g;
    ($partner->{Diligence})           = $html =~ m|\bField Partner Due Diligence Type:</a></dt>\s*<dd>([^<>]+)</dd>|;
    ($partner->{Totalloans})          = $html =~ m|\bTotal Loans:</a></dt>\s*<dd>([^<>]+)</dd>|;

    $html = $dom->find('section')->all_contents;

    ($partner->{PartnerCountry})      = $html =~ m|\bCountry</a>:</dt>\s*<dd>([^<>]+)</dd>|;
    ($partner->{Startdate})           = $html =~ m|\bStart Date On Kiva</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{InactAmt})            = $html =~ m|\bAmount of Raised Inactive Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{InactNo})             = $html =~ m|\bNumber Of Raised Inactive Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{PaybackAmt})          = $html =~ m|\bAmount of Paying Back Loans</a></th>\s*<td>([^<>]+)</td>|; 
    ($partner->{PaybackNo})           = $html =~ m|\bNumber Of Paying Back Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{EndedAmt})            = $html =~ m|\bAmount of Ended Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{EndedNo})             = $html =~ m|\bNumber Of Ended Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{Delinqrate})          = $html =~ m|\bDelinquency Rate</a>\s*</th>\s*<td>([^<>]+)</td>|;
    ($partner->{ArrearsAmt})          = $html =~ m|\bAmount In Arrears</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{OutstndPort})         = $html =~ m|\bOutstanding Portfolio</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{DelinqNo})            = $html =~ m|\bNumber of Loans Delinquent</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{Defaultrate})         = $html =~ m|\bDefault Rate</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{DefEndAmt})           = $html =~ m|\bAmount of Ended Loans Defaulted</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{DefEndNo})            = $html =~ m|\bNumber Of Ended Loans Defaulted</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{CrrXlossRate})        = $html =~ m|\bCurrency Exchange Loss Rate</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{CrrXlossAmt})         = $html =~ m|\bAmount of Currency Exchange Loss</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{RefundRate})          = $html =~ m|\bRefund Rate</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{RefundAmt})           = $html =~ m|\bAmount of Refunded Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{RefundNo})            = $html =~ m|\bNumber Of Refunded Loans</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{WomenLoan})           = $html =~ m|\bLoans To Women Entrepreneurs</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{Avgsize})             = $html =~ m|\bAverage Loan Size</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgInd})              = $html =~ m|\bAverage Individual Loan Size</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgGrp})              = $html =~ m|\bAverage Group Loan Size</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgEntrp})            = $html =~ m|\bAverage Number Of Entrepreneurs Per Group</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgPPPlocal})         = $html =~ m|\bAverage GDP Per Capita \(PPP\) in Local Country</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgsizevsPPP})        = $html =~ m|\bAverage Loan Size / GDP Per Capita \(PPP\)</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgTime2fund})        = $html =~ m|\bAverage Time To Fund A Loan</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgdayPerLoan})       = $html =~ m|\bAverage Dollars Raised Per Day Per Loan</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgloanTerm})         = $html =~ m|\bAverage Loan Term</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{totalJournal})        = $html =~ m|\bTotal Journals</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{JournalRate})         = $html =~ m|\bJournaling Rate</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgCommPerJ})         = $html =~ m|\bAverage Number Of Comments Per Journal</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AcgRecommPerJ})       = $html =~ m|\bAverage Number Of Recommendations Per Journal</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{Pyield})              = $html =~ m|\bPortfolio Yield</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{ROA})                 = $html =~ m|\bProfitability \(Return on Assets\)</a></th>\s*<td>([^<>]+)</td>|;
    ($partner->{AvgsizeperCapIncome}) = $html =~ m|\bAverage Loan Size \(\% of Per Capita Income\)</a></th>\s*<td>([^<>]+)</td>|;

    map { $partner->{$_} = "N/A" unless defined $partner->{$_} } keys %{$partner};

# Country
    my $country_id = $partner->{PartnerCountry};
    my $country = $retval->{country}{$country_id} = {};
    $html = $dom->find('section.countryInfo')->all_contents;
    ($country->{CountryID})   = $country_id;
    ($country->{CountryName}) = $country_id;
    ($country->{Capital})     = $html =~ m|Capital</a>:</dt>\s*<dd>([^<>]+)</dd>|;
    ($country->{OffLang})     = $html =~ m|Official Language</a>:</dt>\s*<dd>([^<>]+)</dd>|; 
    ($country->{Population})  = $html =~ m|Population</a>:</dt>\s*<dd>([^<>]+)</dd>|; 
    ($country->{AnnIncome})   = $html =~ m|Avg Annual Income</a>:</dt>\s*<dd>([^<>]+)</dd>|; 
    ($country->{Agriculture}) = $html =~ m|Agriculture:?\s*(\d[\d.]*\%)|i; 
    ($country->{Industry})    = $html =~ m|Industry:?\s*(\d[\d.]*\%)|i;
    ($country->{Service})     = $html =~ m|Services?:?\s*(\d[\d.]*\%)|i;
    ($country->{Poverty})     = $html =~ m|Population Below Poverty Line</a>:</dt>\s*<dd>([^<>]+)</dd>|;
    ($country->{Literacy})    = $html =~ m|Literacy Rate</a>:</dt>\s*<dd>([^<>]+)</dd>|;
    ($country->{InfantMort})  = $html =~ m|Infant Mortality Rate \(per 1000\)</a>:</dt>\s*<dd>([^<>]+)</dd>|;
    ($country->{Lifeexpct})   = $html =~ m|Life Expectancy</a>:</dt>\s*<dd>([^<>]+)</dd>|;

    map { $country->{$_} = "N/A" unless defined $country->{$_} } keys %{$country};

}

 
print "\n======\nDone.\n";
