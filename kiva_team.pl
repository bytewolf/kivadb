#!/usr/bin/env perl
# hui
# scrapy team's info from kiva.org

use strict;
use Mojo::UserAgent;
use Mojo::DOM;
use FindBin qw/$Bin/;
use JSON::XS;
use DBM::Deep;
use Data::Dumper;
$|=1;

my $datadir = "$Bin/../data";
my $sitemap = 'http://www.kiva.org/sitemaps/index.xml';
my $ua = Mojo::UserAgent->new;
#$ua->transactor->name('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
$ua->transactor->name('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0');

print "-> visiting $sitemap\n";
my $tx = $ua->get($sitemap);
my $html = $tx->res->body;
my @teams_url;
if ($html =~ /\/teams\d*\.xml<\/loc>/) {
    while ($html =~ m|<loc>(http://www\.kiva\.org/[^<>]+/teams\d*\.xml)</loc>|g) {
        push @teams_url, $1; 
    }

} else {
    print Dumper $tx->res;
    die "failed!";
}


my @team_ids;
my $delay = Mojo::IOLoop->delay;
for my $url (@teams_url) {
    my $end = $delay->begin;
    print " -> scrapping team IDs from: $url\n";
    $ua->get($url => sub {
            my ($ua, $tx) = @_;
            my $html = $tx->res->body;
            if ($html =~ m|<loc>http://www\.kiva\.org/team/|) {
                while ($html =~ m|<loc>http://www\.kiva\.org/team/([^<>]+)</loc>|g) {
                    push @team_ids, $1; 
                }

            } else {
                print Dumper $tx->res;
                warn "$url doesn't have team urls!";
                exit;
            }
            $end->();
        }
    );
}
$delay->wait() unless $delay->ioloop->is_running();
my $total = scalar @team_ids;
print "  - [Total team IDs: $total]\n";

### Main

open F, ">$Bin/../,all_team_ids" or die "Can't create $Bin/../,all_team_ids: $!\n";
for my $id (@team_ids) {
    print F "$id\n";
}
close F;

my $i = 0;
my $active = 0;
my %retry;
my @target;
my %result;
my %url_loans;
my %url_members;
tie %result, 'DBM::Deep', "$datadir/team.db";
{
    my $MAX = 20;
    @target = map {'http://www.kiva.org/team/'.$_} @team_ids;
    _parallel_get($MAX, \@target, \&_cb_parseResult, \%result);
}


Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

open TL, ">$Bin/../,url_team_loan" or die "Can't create $Bin/../,url_team_loan: $!\n";
for my $id (keys %url_loans) {
    print TL $url_loans{$id}."\thttp://www.kiva.org/team/$id/loans\n";  
}
close TL;

open TM, ">$Bin/../,url_team_member" or die "Can't create $Bin/../,url_team_member: $!\n";
for my $id (keys %url_members) {
    print TM $url_members{$id}."\thttp://www.kiva.org/team/$id/members\n";  
}
close TM;

### Subs

sub _parallel_get {
    my ( $MAX, $target, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 20 ? $MAX : 10);
    Mojo::IOLoop->recurring(
        0 => sub {

            for ($active + 1 .. $MAX) {

                return($active or Mojo::IOLoop->stop) unless my $url = shift @$target; 
                $active++;
                $ua->get("$url" => sub {
                    my ($ua, $tx) = @_;
                    $callback->($tx, $retval);
                });

            }
        });
}

 
sub _cb_parseResult {
    my ($tx, $retval) = @_;
    my $url  =  $tx->req->url;
    my $html = $tx->res->body;
    my $dom  = $tx->res->dom;
    my ($id) = $url =~ /\/([^\/]+)$/;
    my $team = $retval->{$id} = {};
    $i++;
    $active--;

    print " ($i/$total) -> scrapping team details from: $url\n";
    unless ($html =~ /\bteam since\b/) {
        $i--;
        warn "[err] something wrong with this team page: $url\n";
        if (exists $retry{$url} && $retry{$url} > 3) {
           warn "[err: retried > 3 times - $url\n]"; 
        } else {
            unshift @target, $url;
            $retry{$url}++;
        }
        return "err";
    }

    $team->{TeamID} = $id;
    ($team->{TeamName}) = $html =~ m|\bKiva Lending Team:\s*([^<>]+?)\s*</h|; 
    ($team->{Category}) = $html =~ m|/teams\?category=[^<>]+>([^<>]+)</a>|;
    ($team->{Since})    = $html =~ m|\bteam since (\w{3} \d{1,2}, \d{4})|;

    $html = $dom->at('section#teamProfile')->contents;
    $team->{Because} = "N/A";
    if ($html =~ /\bWe loan because\b/) {
        $team->{Because} = $dom->at('div.profileStats > p')->text;
    }

    $team->{About} = "N/A";
    if ($html =~ /\bAbout us\b/) {
        ($team->{About}) = $html =~ m|\bAbout us</span>\s*(.+?)\s*</p>|s;
        my $dom_tmp = Mojo::DOM->new->parse($team->{About});
        $team->{About} = $dom_tmp->all_text(0);
        $team->{About} =~ s/ +/ /g;
    }

    $team->{Location} = "N/A";
    if ($html =~ /\bLocation:/) {
        ($team->{Location}) = $html =~ m|Location:</span>\s*([^<>]+)\s*<|s;
        $team->{Location} =~ s/\xA0+//g;
        $team->{Location} =~ s/\s+/ /g;
    }

    $html = $dom->find('div.rightCol > section')->all_contents;
    ($team->{Amountloaned}) = $html =~ m|>([^<>]+)</div>\s*<div class="impactType">Amount Loaned<|s;
    ($team->{Noofmembers})  = $html =~ m|>([^<>]+)</div>\s*<div class="impactType">Team Members<|s;
    ($team->{Noofloans})    = $html =~ m|>([^<>]+)</div>\s*<div class="impactType">Loans<|s;
    map {$team->{$_} =~ s/,//g} qw/Amountloaned Noofmembers Noofloans/;

    print "    -[ team $id members: ".$team->{Noofmembers}." loans: ".$team->{Noofloans}." ]\n";
    $url_loans{$id} = $team->{Noofloans};
    $url_members{$id} = $team->{Noofmembers};

    map { $team->{$_} = "N/A" } qw/ARankNUAll ARankNUThisMon ARankNULastMon
                                    ARankALAll ARankALThisMon ARankALLastMon
                                    CRankNUAll CRankNUThisMon CRankNULastMon
                                    CRankALAll CRankALThisMon CRankALLastMon/;

    if ($html =~ /\bRankings Across All\b/) {
        ($team->{ARankNUAll})     = $html =~ m|\#(\d+) for New Users \(All Time\)|;
        ($team->{ARankNUThisMon}) = $html =~ m|\#(\d+) for New Users \(This Month\)|;
        ($team->{ARankNULastMon}) = $html =~ m|\#(\d+) for New Users \(Last Month\)|;
        ($team->{ARankALAll})     = $html =~ m|\#(\d+) for Amount Loaned \(All Time\)|;
        ($team->{ARankALThisMon}) = $html =~ m|\#(\d+) for Amount Loaned \(This Month\)|;
        ($team->{ARankALLastMon}) = $html =~ m|\#(\d+) for Amount Loaned \(Last Month\)|;
    }

    if ($html =~ /\bRankings In Common\b/) {
        ($team->{CRankNUAll})     = $html =~ m|\#(\d+) for New Users \(All Time\)|;
        ($team->{CRankNUThisMon}) = $html =~ m|\#(\d+) for New Users \(This Month\)|;
        ($team->{CRankNULastMon}) = $html =~ m|\#(\d+) for New Users \(Last Month\)|;
        ($team->{CRankALAll})     = $html =~ m|\#(\d+) for Amount Loaned \(All Time\)|;
        ($team->{CRankALThisMon}) = $html =~ m|\#(\d+) for Amount Loaned \(This Month\)|;
        ($team->{CRankALLastMon}) = $html =~ m|\#(\d+) for Amount Loaned \(Last Month\)|;
    }

}


print "\n======\nDone.\n";
