#!/usr/bin/env perl
# convert kiva_lend.pl generated data to CSV for later importing to AccessDB.
use Text::CSV;
use 5.010;
use warnings;
use Encode qw/_utf8_off/;
use YAML qw(Dump DumpFile);
use JSON qw(to_json from_json);
use POSIX qw/strftime/;
use Data::Dumper;


#use perl -MData::Dumper -MDBM::Deep -le 'tie %h,"DBM::Deep", "loan.db"; print Dumper $h{687689}'
use DBM::Deep;

my $db_file = shift;

# Predisbursed need change
my $mapping = {
    # Country => [
    #     qw( CountryID CountryName Capital OffLang Population AnnIncome
    #       Agriculture Industry Service Poverty Literacy InfantMort Lifeexpct )
    # ],
    Loan => [
        qw(LoanID LoanName Gender LoanAmount GroupYN NoinGroup LoanCategory1 LoanCategory2),
        qw(RepayTerm RepaySchedule Pre-disbursed Listed Currencyloss DefaultProtect Ended PictureYN),
        qw(LoanDescription AdditionalInfo Translator OriginalLang CountryID PartnerID),
        qw(Lenders Teams)

    ],
    # LoanGroup  => [qw( LoanID GrpmemberName )],
    # LoanMember => [qw( LoanID MemberID )],
    # Member     => [
    #     qw( MemberID MemberName location occupation because about MemberSince picture )
    # ],
    # Partner    => [],
    # SocialPerf => [qw(PartnerID SocialBadge )],
    # Team       => [],
    # TeamLoan   => [qw(TeamID LoanID)],
    # TeamMember => [qw(TeamID MemberID Captain)],
};

my $db = DBM::Deep->new($db_file);
my $key = ( keys %$db )[1];
say "key is $key";
#say Dump $hash{$key};

#open $fh, ">:encoding(utf8)", "new.csv" or die "new.csv: $!";
my $csv = Text::CSV->new( { binary => 1 ,sep_char => ","} );
$csv->eol("\r\n");

my $fh_set = {};
for my $t ( keys %$mapping) {
    open( my $fh, ">:encoding(utf8)", '/tmp/'.$t . ".csv" );
    my @columns = @{ $mapping->{$t} };
    $fh_set->{$t} = $fh;
    print $fh join(",",@columns)."\n";
}
my $json = JSON->new;
#$json->allow_blessed(1);
#$json->allow_nonref(1);
#$json->convert_blessed(1);
mapping_access($db);

sub mapping_access {
    my $hash = shift;
    my $count = 0;

    for my $id ( keys %$hash) {
        for my $t ( keys %$mapping ) {
            my @row;
            for ( @{ $mapping->{$t} } ) {
                my $field = $_;
                $field =~ s/-//g;
                $field = /^(Lenders|Teams)$/ ? lcfirst($field) : $field;

                my $v = 'N/A';
                if (($field =~ /^(lenders|teams)$/) && ((ref $hash->{$id}{$field}) =~ /Array/)) {
                    $v = join ",", map { $_->{profile} } @{ $hash->{$id}{$field} };
                    $v =~ s/http:\/\/[^\s,]+\/\b//g;
                } elsif ($field =~ /^(Predisbursed|Listed)$/) {
                    $v = $hash->{$id}{$field} ? strftime("%Y-%m-%d", gmtime($hash->{$id}{$field})) : $v;
                } else {
                    $v = $hash->{$id}{$field} ? $hash->{$id}{$field} : $v; 
                }

                $v =~ s/\s*USD// if $field eq "LoanAmount";
                $v =~ s/^\s*|\s*$//g;
                $v =~ s/[\r\n]+/ /g;

                _utf8_off $v;
                #my $v = ref $raw ? $json->encode($raw->export() ) : $raw;
                push @row, $v;
            }
            my $fh = $fh_set->{$t};
            #print $fh join(',',map { '"'.$_.'"' } @row)."\n" if @row;
            $csv->print($fh,[@row]) if @row;
        }
        $count++;
    }
    close $fh_set->{$_} for keys %$fh_set;
}

__END__

open $fh, ">:encoding(utf8)", "new.csv" or die "new.csv: $!";
