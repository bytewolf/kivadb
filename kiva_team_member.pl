#!/usr/bin/env perl
# hui
# scrapy team's member info from kiva.org

use strict;
use Mojo::UserAgent;
use Mojo::DOM;
use FindBin qw/$Bin/;
use JSON::XS;
use DBM::Deep;
use Data::Dumper;
$|=1;

my $datadir = "$Bin/../data";
my $url_team_member = "$Bin/../,url_team_member";

my $total = 0;
my @target;
tie my %result, 'DBM::Deep', "$datadir/teammember.db";
open F, $url_team_member or die "Can't open $url_team_member: $!\n";
while (my $line = <F>) {
    chomp $line;
    my ($member_no, $url) = split /\t/, $line;
    my ($id) = $url =~ /\/([^\/]+)\/loans$/;
    if ($member_no > 0) { 
        push @target, $url;
        $total++;
    }
}
print " -> There are $total teams have members.\n";


my $ua = Mojo::UserAgent->new;
#$ua->transactor->name('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
$ua->transactor->name('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0');

my $i = 0;
my $active = 0;
my %done;
my %retry;
{
    my $MAX = 20;
    _parallel_get($MAX, \@target, \&_cb_parseResult, \%result);
}

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;
my $i = 0;
open F, ">$Bin/../,all_members" or die "Can't create $Bin/../,all_members: $!\n";
for my $id (keys %result) {
        print F "$id\n";
        $i++;
}
close F;

print " -> Total members: $i\n";


### Subs

sub _parallel_get {
    my ( $MAX, $target, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 20 ? $MAX : 10);
    Mojo::IOLoop->recurring(
        0 => sub {

            for ($active + 1 .. $MAX) {

                return($active or Mojo::IOLoop->stop) unless my $url = shift @$target; 
                $active++;
                $ua->get("$url" => sub {
                    my ($ua, $tx) = @_;
                    $callback->($tx, $retval);
                });

            }
        });
}

 
sub _cb_parseResult {
    my ($tx, $retval) = @_;
    my $url  = $tx->req->url;
    my $html = $tx->res->body;
    my $dom  = $tx->res->dom;
    my ($id) = $url =~ /\/([^\/]+)\/members(?:\?|$)/;
    my $member = $retval;
    $i++;
    $active--;

    print " ($i/$total) -> scrapping team member info from: $url\n";
    unless ($html =~ /\bTeam Members\b/) {
        $i--;
        warn "[err] something wrong with this team page: $url\n";
        if (exists $retry{$url} && $retry{$url} > 3) {
           warn "[err: retried > 3 times - $url\n]"; 
        } else {
            unshift @target, $url;
            $retry{$url}++;
        }
        return "err";
    }

    my $dom_captain = $dom->at('section#captains ul');
    if ($dom_captain) {
        $dom_captain->find('div.lenderCard')->each(
            sub { my $e = shift;
                if ($e->at('div.name')->text eq "Anonymous") {
                    push @{$member->{Anonymous}{isCaptian}}, $id;
                } else {
                    my $url = $e->at('a')->attr('href'); 
                    my ($lender) = $url =~ /\/lender\/([^\/]+)$/;
                    push @{$member->{$lender}{isCaptian}}, $id;
                } 
            } 
        ); 
    }

    if (not exists $done{$id}) {
        my $html = $dom->at('div.kvpager')->contents;
        my ($last_page) = $html =~ m|\btitle="last page">(\d+)</a>|; 

        if (!$last_page) {
            if ($html =~ m|\bNext Page\b|) {
                my @pages = $dom->find('div.kvpager a')->each; 
                $last_page = scalar @pages;
             } else {
                $last_page = 1; 
             }
        }        

        if ($last_page > 1) {
            map { push @target, "http://www.kiva.org/team/$id/members?pageID=$_"; $total++ } (2..$last_page);
            $done{$id}++;
        }

        print "    - [ team: $id has $last_page pages of member info ]\n";
    }

   my $dom_lender = $dom->at('section#members ul'); 
   if ($dom_lender) {
        $dom_lender->find('div.lenderCard')->each(
            sub { my $e = shift;
                if ($e->at('div.name')->text eq "Anonymous") {
                    push @{$member->{Anonymous}{teams}}, $id;
                } else {
                    my $url = $e->at('a')->attr('href'); 
                    my ($lender) = $url =~ /\/lender\/([^\/]+)$/;
                    push @{$member->{$lender}{teams}}, $id;
                } 
            } 
        ); 
   }

}


print "\n======\nDone.\n";
