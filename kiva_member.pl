#!/usr/bin/env perl
# hui
# scrapy member detail from kiva.org

use strict;
use Mojo::UserAgent;
use Mojo::DOM;
use Mojo::ByteStream 'b';
use JSON::XS;
use FindBin qw/$Bin/;
use DBM::Deep;
use Data::Dumper;
$|=1;

my $datadir = "$Bin/../data";
my $all_member = "$Bin/../,all_members";

my $total = 0;
my @target;
open F, $all_member or die "Can't open $all_member: $!\n";
while (my $line = <F>) {
    chomp $line;
    my $id = $line;
    push @target, $id;
    $total++;
}
print " -> There are $total members.\n";


my $ua = Mojo::UserAgent->new;
#$ua->transactor->name('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
$ua->transactor->name('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0');

tie my %result, 'DBM::Deep', "$datadir/member.db";
my $i = 0;
my $active = 0;
my %done;
my %retry;
{
    my $MAX = 30;
    map { $_ = "http://www.kiva.org/lender/$_" } @target;
    _parallel_get($MAX, \@target, \&_cb_parseResult, \%result);
}

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;


### Subs

sub _parallel_get {
    my ( $MAX, $target, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 30 ? $MAX : 10);
    Mojo::IOLoop->recurring(
        0 => sub {

            for ($active + 1 .. $MAX) {

                return($active or Mojo::IOLoop->stop) unless my $url = shift @$target; 
                $active++;
                $ua->get("$url" => sub {
                    my ($ua, $tx) = @_;
                    $callback->($tx, $retval);
                });

            }
        });
}

 
sub _cb_parseResult {
    my ($tx, $retval) = @_;
    my $url  = $tx->req->url;
    my $html = $tx->res->body;
    my $dom  = $tx->res->dom;
    my ($id) = $url =~ /\/([^\/]+)$/;
    my $member = $retval->{$id} = {};
    $i++;
    $active--;

    print " ($i/$total) -> scrapping team detail from: $url\n";
    my $json;
    if ($html =~ /\bvar Lender_LenderView\s*=\s*({.+})\s*;/) {
        $json = decode_json($1);
        $json = $json->{lender};
    } else {
        $i--;
        warn "[err] something wrong with this member page: $url\n";
        if (exists $retry{$url} && $retry{$url} > 3) {
           warn "[err: retried > 3 times - $url\n]"; 
        } else {
            unshift @target, $url;
            $retry{$url}++;
        }
        return "err";
    }

    $member->{MemberID}    = $id; 
    $member->{MemberName}  = $json->{name};
    $member->{location}    = exists $json->{lenderPage}{whereabouts} ? $json->{lenderPage}{whereabouts} : "N/A";
    $member->{occupation}  = exists $json->{lenderPage}{occupation} ? $json->{lenderPage}{occupation} : "N/A";
    $member->{because}     = exists $json->{lenderPage}{loanBecause} ? $json->{lenderPage}{loanBecause} : "N/A";
    $member->{about}       = exists $json->{lenderPage}{otherInfo} ? $json->{lenderPage}{otherInfo} : "N/A";
    $member->{MemberSince} = $json->{memberSince};
    $member->{picture}     = "http://www.kiva.org/img/w200h200/".$json->{image}{id}.".jpg";

}


print "\n======\nDone.\n";
