CREATE TABLE `Member` (
      `MemberID` varchar(30) NOT NULL,
      `MemberName` varchar(30) DEFAULT NULL,
      `location` varchar(40) DEFAULT NULL,
      `occupation` varchar(30) DEFAULT NULL,
      `because` varchar(255) DEFAULT NULL,
      `MemberSince` varchar(30) DEFAULT NULL,
      `about` varchar(40) DEFAULT NULL,
      `picture` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`MemberID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Partner`(
      `PartnerID` varchar(30) NOT NULL,
      `PartnerName` varchar(30) DEFAULT NULL,
      `RiskRating` int(20) DEFAULT NULL,
      `PartnerCountry` varchar(30) DEFAULT NULL,
      `Dilligence` varchar(40) DEFAULT NULL,
      `Startdate` varchar(30) DEFAULT NULL,
      `Totalloans` varchar(40) DEFAULT NULL,
      `InactAmt` varchar(255) DEFAULT NULL,
      `InactNo` int(20) DEFAULT NULL,
      `PaybackAmt` varchar(255) DEFAULT NULL,
      `PaybackNo` int(25) DEFAULT NULL,
      `EndedAmt` varchar(255) DEFAULT NULL,
      `EndedNo` int(30) DEFAULT NULL,
      `Delinqrate` int(20) DEFAULT NULL,
      `ArrearsAmt` varchar(25) DEFAULT NULL,
      `OutstndPort` varchar(25) DEFAULT NULL,
      `DelinqNo` int(30) DEFAULT NULL,
      `Defaultrate` int(30) DEFAULT NULL,
      `DefEndAmt` varchar(30) DEFAULT NULL,
      `DefEndNo` int(40) DEFAULT NULL,
      `CrrXlossRate` int(40) DEFAULT NULL,
      `CrrXlossAmt` varchar(30) DEFAULT NULL,
      `RefundRate` int(30) DEFAULT NULL,
      `RefundAmt` varchar(30) DEFAULT NULL,
      `RefundNo` int(30) DEFAULT NULL,
      PRIMARY KEY (`PartnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `SocialPerf` (
      `PartnerID` varchar(255) NOT NULL,
      `SocialBadge` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`PartnerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LoanMember` (
      `LoanID` varchar(255) NOT NULL,
      `MemberID` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`LoanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Loan` (
      `LoanID` varchar(255) NOT NULL,
      `LoanName` varchar(255) DEFAULT NULL,
      `Gender` varchar(100) DEFAULT NULL,
      `LoanAmount` varchar(30) DEFAULT NULL,
      `GroupYN` varchar(10) DEFAULT NULL,
      `NoinGroup` int(20) DEFAULT NULL,
      `LoanCategory1` varchar(255) DEFAULT NULL,
      `LoanCategory2` varchar(255) DEFAULT NULL,
      `RepayTerm` int(20) DEFAULT NULL,
      `RepaySchedule` text DEFAULT NULL,
      `Pre-disbursed` varchar(40) DEFAULT NULL,
      `Listed` varchar(40) DEFAULT NULL,
      `Currencyloss` text DEFAULT NULL,
      `DefaultProtection` text DEFAULT NULL,
    `Ended` varchar(40) DEFAULT NULL,
    `PictureYN` varchar(40) DEFAULT NULL,
    `LoanDescription` text DEFAULT NULL,
    `AdditionalInfo` text DEFAULT NULL,
    `Translator` varchar(255) DEFAULT NULL,
    `OriginalLang` varchar(255) DEFAULT NULL,
    `CountryID` varchar(255) DEFAULT NULL,
    `PartnerID` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`LoanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `LoanGroup` (
      `LoanID` varchar(255) NOT NULL,
      `GrpmemberName` text DEFAULT NULL,
      PRIMARY KEY (`LoanID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Team` (
      `TeamID` varchar(255) NOT NULL,
      `TeamName` varchar(255) NOT NULL,
      `Category` varchar(255) NOT NULL,
      `Since` varchar(255) NOT NULL,
      `Because` varchar(255) NOT NULL,
      `About` varchar(255) NOT NULL,
      `Location` varchar(255) NOT NULL,
      `Noofmembers` int(20) NOT NULL,
      `Amountloaned` int(20) NOT NULL,
      `Noofloans` int(20) NOT NULL,
      `ARankNUAll` int(20) NOT NULL,
      `ARankNUThisMon` int(20) NOT NULL,
      `ARankNULastMon` int(20) NOT NULL,
      `ARankALAll` int(20) NOT NULL,
      `ARankALThisMon` int(20) NOT NULL,
      `ARankALLastMon` int(20) NOT NULL,
      `CRankNUAll` int(20) NOT NULL,
      `CRankNUThisMon` int(20) NOT NULL,
      `CRankNULastMon` int(20) NOT NULL,
      `CRankALAll` int(20) NOT NULL,
      `CRankALThisMon` int(20) NOT NULL,
      `CRankALLastMon` int(20) NOT NULL,
      PRIMARY KEY (`TeamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `TeamMember` (
      `TeamID` varchar(255) NOT NULL,
      `MemberID` varchar(255) DEFAULT NULL,
      `Captain` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`TeamID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





















