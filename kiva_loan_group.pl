#!/usr/bin/env perl
use Text::CSV;
use FindBin qw($Bin);
use 5.010;
use warnings;
use Encode qw/_utf8_off/;
use YAML qw(Dump DumpFile);
use Data::Dumper;
use AnyEvent;
use Coro;
use Coro::Semaphore;
use Mojo::UserAgent;

#use perl -MData::Dumper -MDBM::Deep -le 'tie %h,"DBM::Deep", "loan.db"; print Dumper $h{687689}'
use DBM::Deep;

$|++;

my $db_file = shift;
my $db      = DBM::Deep->new($db_file);
my $key     = ( keys %$db )[1];

my $datadir = "$Bin/../data";
my %loan_group;
my %fail_url;
tie %loan_group, 'DBM::Deep', "$datadir/loan_group.db";

my $ua = Mojo::UserAgent->new;
$ua->transactor->name(
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0'
);
my $sem = Coro::Semaphore->new(20);

my $cv = AnyEvent->condvar;

while ( my ( $k, $v ) = each %$db ) {
    my $lend_url = "http://www.kiva.org/lend/" . $k;

    next unless exists $db->{$k}->{GroupYN};
    next unless $db->{$k}->{GroupYN} eq 'Y';
    async_request( $lend_url, $db->{$k} );
}

$cv->recv;

sub is_downloaded {
    return exists $fail_url{+shift} ? 1 : 0;
}

sub async_request {
    my ( $url, $attr ) = @_;

    if ($url) {
        async_pool {
            $sem->down;
            $cv->begin;
            $ua->get(
                $url => sub {
                    my ( $ua, $tx ) = @_;

                    my $url = $tx->req->url->to_string;
                    say "Begin request url => $url";

                    unless ( $tx->success ) {
                        $fail_url{$url} = 1;
                    }
                    my $dom = $tx->res->dom;
                    if (
                        my $e = $dom->at(
'div#loanProfileTabs.tabs div.loanViewTabbedContent section#businessProfile.ui-tabs-panel div.g8 figure.businessFig figcaption'
                        )
                      )
                    {
                      #$loan_group->{loan_group}{load_id}    = $attr->{load_id};
                        $loan_group{loan_group}{ $attr->{LoanID} }{LoanID} = $attr->{LoanID};
                        ($loan_group{loan_group}{ $attr->{LoanID} }{GrpmemberName}) = $e->all_text =~ m/Group:\s*(.*)/six;
                        ($loan_group{loan_group}{ $attr->{LoanID} }{GrpmemberName}) =~ s/^\s+//g;
                        ($loan_group{loan_group}{ $attr->{LoanID} }{GrpmemberName}) =~ s/\s+$//g;
                        say " parsed group_info => " .$loan_group{loan_group}{ $attr->{LoanID} }{GrpmemberName};
                    }

                    $sem->up;
                    $cv->end;
                }
            );
        };
    }
}

exit;

