#!/usr/bin/env perl
# hui
# scrapy lend's info from kiva.org

use strict;
use Mojo::UserAgent;
use FindBin qw/$Bin/;
use JSON::XS;
use DBM::Deep;
use Data::Dumper;
$|=1;

my $datadir = "$Bin/../data";
tie my %loan_details, 'DBM::Deep', "$datadir/loan.db";

my $url = 'http://www.kiva.org/lend';
my $ua = Mojo::UserAgent->new;
#$ua->transactor->name('Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)');
$ua->transactor->name('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:28.0) Gecko/20100101 Firefox/28.0');

print "-> getting pages from $url\n";
my $tx = $ua->get($url);
my $html = $tx->res->body;
my $total = 0;
my $per_page = 100;
my $total_page = 1;

if ($html =~ /var\s+Lend_ListingView\s/) {
    my ($lends) = $html =~ /\bvar\s+Lend_ListingView\s+=\s+(\{.+\});/;
    my $json = decode_json($lends);
    $total = $json->{results}{n_businesses};
    $total_page = int($total/$per_page) + 1;
} else {
    print Dumper $tx->res;
    die "failed!";
}
print "  - [total: $total per_page: $per_page total_page: $total_page]\n";


### Main

my @loan_ids;
{
    my $MAX = 10;
    #my @target = map {'http://www.kiva.org/lend/queryBusinessesListing?pageID='.$_.'&perPage='.$per_page} 1..$total_page;
    my @requests = map {
        [
            "http://www.kiva.org/lend/queryBusinessesListing",
            {DNT=>1},
            form => {pageID => $_, perPage => $per_page}
        ]
    } 1..$total_page;
    
    _parallel_post($MAX, \@requests, \&_cb_parseLendLink, \@loan_ids);
}

open F, ">$Bin/../,all_loan_ids" or die "Can't create $Bin/../,all_loan_ids: $!\n";
for my $id (@loan_ids) {
    print F "$id\n";
}
close F;

our @target;
our %lenders;
%loan_details;
{
    my $MAX = 10;
    @target = map {'http://www.kiva.org/lend/'.$_} @loan_ids;
    _parallel_get($MAX, \@target, \&_cb_parseLendDetail, \%loan_details);
}


{
    my $MAX = 10;
    my @requests = map { $lenders{$_} } keys %lenders;
    _parallel_post($MAX, \@requests, \&_cb_parseLenders, \%loan_details);
}


{
    my $MAX = 10;
    @target = map {'http://www.kiva.org/ajax/callLoan_TopLendingTeamSubView?&show_all_teams=1&includeCssAndJs=0&biz_id='.$_} @loan_ids;
    _parallel_get($MAX, \@target, \&_cb_parseLendTeams, \%loan_details);
}



print "Total loan ids: ".(scalar keys %loan_details)."\n";



### Subs

sub _parallel_get {
    my ( $MAX, $target, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 10 ? $MAX : 10);
    my $i = 0;
    my @batch;
    for my $item (@$target) {
        $i++;
        push @batch, $item;

        if ($i == @$target || !($i % $MAX)) {

            my $delay = Mojo::IOLoop->delay;

            for my $url (@batch) {
                my $end = $delay->begin(0); 

                $ua->get("$url" => sub {
                        my ($ua, $tx) = @_;
                        $callback->($tx, $retval);
                        $end->();
                    });
            }

            $delay->wait;
            undef @batch;
        }

    }
}


sub _parallel_post {
    my ( $MAX, $requests, $callback, $retval) = @_;
    $ua->max_connections($MAX <= 10 ? $MAX : 10);
    my $i = 0;
    my @batch;
    for my $item (@$requests) {
        $i++;
        push @batch, $item;

        if ($i == @$requests || !($i % $MAX)) {

            my $delay = Mojo::IOLoop->delay;

            for my $req (@batch) {
                my $end = $delay->begin(0); 

                $ua->post(@$req => sub {
                        my ($ua, $tx) = @_;
                        $callback->($tx, $retval);
                        $end->();
                    });
            }

            $delay->wait;
            undef @batch;
        }

    }
}


sub _cb_parseLendLink {
    my ($tx, $retval) = @_;
    my $json;
    my $url = $tx->req->url;
    my $page = $tx->req->params->param('pageID');
    my $html = $tx->res->body;
    my $dom = $tx->res->dom;

    print " -> scrapping loan IDs from $url, page=$page\n";
    if ($html =~ /"business_ids":\s*\[([^\[\]]+)\]/) {
        my @ids = split /,/, $1;
        push @$retval, @ids;
        my $n = scalar @ids;

    } elsif ($html =~ /"business_ids":\s*\{([^{}]+)\}/) {
        my @ids = map { (split /:/, $_)[1] } split /,/, $1;
        push @$retval, @ids;
        my $n = scalar @ids;
    
    } else {
        warn "[err] can't find business_ids in: $url\n";
        return "err";
    }

}


sub _cb_parseLendDetail {
    my ($tx, $retval) = @_;
    my $url = $tx->req->url;
    my $html = $tx->res->body;
    my $dom = $tx->res->dom;

    print "  -> scrapping loan details from $url\n";
    my ($loanFigureSubView) = $html =~ /\bvar\s+Loan_LoanFigureSubView\s+=\s+(\{.+\});/;
    
    if (!$loanFigureSubView) {
        warn "[RETRY] $url\n"; 
        unshift @target, $url;
        return "err";
    }

    my $json =  decode_json($loanFigureSubView);
    my $id = $json->{loan}{id};
    my $orignal_lang = $json->{loan}{descriptionOriginalLanguage} || "N/A";
    # open F, ">$datadir/$id.json" or die "Can't create $datadir/$id.json: $!\n";
    $loan_details{$id} = {
        LoanID          => $json->{loan}{id},
        LoanName        => $json->{loan}{name},
        Gender          => @{$json->{loan}{borrowers}} > 1 ? 'isGroup' : $json->{loan}{borrowers}[0]->{gender},
        LoanAmount      => join(" ", @{ $json->{loan}{loanAmount} }{ qw/amount currency/ }),
        GroupYN         => @{$json->{loan}{borrowers}} > 1 ? 'Y' : 'N',
        NoinGroup       => scalar @{ $json->{loan}{borrowers} },
        LoanCategory1   => $json->{loan}{sector}{name} || "N/A",
        LoanCategory2   => $json->{loan}{activity}{name} || "N/A",
        RepayTerm       => $json->{loan}{terms}{lenderRepaymentTerm},
        Predisbursed    => $json->{loan}{disbursalDate},
        Listed          => $json->{loan}{fundraisingDate},
        Ended           => $json->{loan}{endedDate} || "N/A",
        PictureYN       => exists $json->{loan}{image}{id} ? 'Y' : 'N',
        LoanPicture     => exists $json->{loan}{image}{id} ? "http://www.kiva.org/img/w800/".$json->{loan}{image}{id}.".jpg" : "N/A",
        LoanDescription => exists $json->{loan}{description}{en} ? $json->{loan}{description}{en} : $json->{loan}{description}{$orignal_lang},
        Translator      => exists $json->{loan}{reviewer}{bylineName} || "N/A",
        OriginalLang    => $orignal_lang,
        CountryID       => $json->{loan}{location}{country}{id},
        CountryCode     => $json->{loan}{location}{country}{isoCode},
        CountryName     => $json->{loan}{location}{country}{name},
        CountryRegion   => $json->{loan}{location}{country}{region},
        PartnerID       => exists $json->{loan}{partnerId} || "N/A",
        PartnerName     => exists $json->{loan}{partnerName} || "N/A",
    };

    my $text = "N/A";
    if ($html =~ /additionalLoanInformation/) {
        my $text  = sprintf "%s", $dom->find('section#additionalLoanInformation')->all_text(0);
        $text =~ s/[ \t]+/ /g;
    }

    ($loan_details{$id}->{AdditionalInfo})  = $text;
    ($loan_details{$id}->{RepaySchedule})   = $html =~ /\bRepayment Schedule:<\/a><\/dt>\s*<dd>([^<>]+)<\/dd>/m;
    ($loan_details{$id}->{Currencyloss}  )  = $html =~ /\bCurrency Exchange Loss:<\/a><\/dt>\s*<dd>([^<>]+)<\/dd>/m;
    ($loan_details{$id}->{DefaultProtect})  = $html =~ /\bDefault Protection<\/a><\/dt>\s*<dd>([^<>]+)<\/dd>/m;

    # my $string =  encode_json(\%hash);
    # print F $string;
    # close F;

    # get lenders and teams info

    if ($html =~ /\bvar\s+Loan_LoanView\s*=\s*(\{.+?lendersJSON.+\});/) {
        my $json = decode_json($1);        
        my $lendersJSON = $json->{lendersJSON};

        $lenders{$id} = [
            "http://www.kiva.org/ajax/callLoan_TopLendersSubView?",
            {DNT => 1},
            form => {
                biz_id => $id,
                lendersJSON => $lendersJSON,
                show_all_lenders => 1,
                if_not_all_show_this_many => 50,
                includeCssAndJs => 0
            }
        ];
    }
}

sub _cb_parseLenders {
    my ($tx, $retval) = @_;
    my $url = $tx->req->url;
    my $id = $tx->req->params->param('biz_id');
    my $html = $tx->res->body;
    my $dom = $tx->res->dom;

    print "  -> scrapping lenders info for $id\n";
    $dom->find('div.name > a')->each(
        sub {
            my $r = shift;
            push @{$loan_details{$id}->{lenders}}, { profile => $r->attr("href"), name => $r->text };
        } 
    );
}

sub _cb_parseLendTeams {
    my ($tx, $retval) = @_;
    my $url = $tx->req->url;
    my $id = $tx->req->params->param('biz_id');
    my $html = $tx->res->body;
    my $dom = $tx->res->dom;

    print "  -> scrapping teams info for $id\n";
    $dom->find('h1.name > a')->each(
        sub {
            my $r = shift;
            push @{$loan_details{$id}->{teams}}, { profile => $r->attr("href"), name => $r->attr("title") };
        } 
    );
}


print "\n======\nDone.\n";



